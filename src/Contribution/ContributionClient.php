<?php

declare(strict_types = 1);

namespace Drupal\drupal_contributions\Contribution;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ContributionClient.
 *
 * Fetches projects from organization's page on Drupal.org.
 */
class ContributionClient implements ContributionClientInterface {

  protected const DRUPAL_ORG_BASE_URL = 'https://www.drupal.org';
  protected const URL = 'https://www.drupal.org/node/%d/issue-credits';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new ContributionClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchProjects($identifier): array {
    $identifier = intval($identifier);
    if (!is_int($identifier) || empty($identifier)) {
      throw new \InvalidArgumentException('Identifier must be of type int or the identifier is invalid.');
    }

    $page = 0;
    $projects = [];

    do {
      $response = $this->httpClient->request(
        'GET',
        sprintf(static::URL, $identifier),
        [
          RequestOptions::QUERY => ['page' => $page++],
          RequestOptions::CONNECT_TIMEOUT => 20,
          RequestOptions::TIMEOUT => 60,
        ]
      );

      $html = (string) $response->getBody();
      $crawler = new Crawler($html);

      $viewContentElements = $this->filterViewContentElements($crawler);

      if ($viewContentElements === NULL) {
        break;
      }

      $projectHeaderElements = iterator_to_array(
        $this->filterProjectHeaderElements($viewContentElements)
      );
      $projectListElements = iterator_to_array(
        $this->filterProjectListElements($viewContentElements)
      );

      for ($i = 0; $i < count($projectHeaderElements); $i++) {
        $headerElement = $listElement = NULL;

        if (!empty($projectHeaderElements[$i])) {
          $headerElement = new Crawler($projectHeaderElements[$i]);
        }

        if (!empty($projectListElements[$i])) {
          $listElement = new Crawler($projectListElements[$i]);
        }

        if (!$headerElement || !$listElement) {
          continue;
        }

        $issues = [];
        $itemElements = $listElement->children()->filter('li');
        foreach ($itemElements as $itemElement) {
          $issueLink = $this->filterIssueLinkElement(
            new Crawler($itemElement)
          );
          $dateElement = $this->filterIssueDateElement(
            new Crawler($itemElement)
          );

          $issueId = $this->extractIssueId($issueLink->attr('href'));
          if (!$issueId) {
            continue;
          }

          $issues[] = [
            'id' => $issueId,
            'url' => static::DRUPAL_ORG_BASE_URL . $issueLink->attr('href'),
            'title' => $issueLink->text(),
            'date' => $dateElement->text(),
          ];
        }

        $projectLink = $this->filterProjectLinkElement($headerElement);
        $projectName = $projectLink->text();
        $projectUrl = $projectLink->attr('href');

        if (!empty($projects[$projectName]['issues'])) {
          // Merge the list If issues are presented on multiple pages.
          $issues = array_merge($projects[$projectName]['issues'], $issues);
        }
        $projects[$projectName] = [
          'url' => static::DRUPAL_ORG_BASE_URL . $projectUrl,
          'issues' => $issues,
        ];
      }

      // Sleep for 2 seconds so we don't hit a page request limit.
      sleep(2);
    } while (TRUE);

    return $projects;

  }

  /**
   * Filters the elements of the scraped content.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler object.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   The crawled object.
   */
  protected function filterViewContentElements(Crawler $crawler): ?Crawler {
    try {
      $viewContentElements = $crawler
        ->filter('.view-issue-credit .view-content')
        ->children();
    }
    catch (\InvalidArgumentException $e) {
      return NULL;
    }

    return $viewContentElements;
  }

  /**
   * Filters the h3 elements of the scraped content.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler object.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   The crawled object.
   */
  protected function filterProjectHeaderElements(Crawler $crawler): ?Crawler {
    return $crawler->filter('h3');
  }

  /**
   * Filters the ul elements of the scraped content.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler object.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   The crawled object.
   */
  protected function filterProjectListElements(Crawler $crawler): ?Crawler {
    return $crawler->filter('ul');
  }

  /**
   * Filters the anchor elements of the scraped content.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler object.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   The crawled object.
   */
  protected function filterProjectLinkElement(Crawler $crawler): ?Crawler {
    return $crawler->children()
      ->filter('a')
      ->first();
  }

  /**
   * Filters the views field a elements of the scraped content.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler object.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   The crawled object.
   */
  protected function filterIssueLinkElement(Crawler $crawler): ?Crawler {
    return $crawler->filter('.views-field-title .field-content a');
  }

  /**
   * Filters the views field elements of the scraped content.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler object.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   The crawled object.
   */
  protected function filterIssueDateElement(Crawler $crawler): ?Crawler {
    return $crawler->filter('.views-field-changed .field-content');
  }

  /**
   * Extracts the issue ID from the scraped content.
   *
   * @param string $url
   *   The url of the issue.
   *
   * @return mixed
   *   Matches or null.
   */
  protected function extractIssueId(string $url): ?int {
    if (!preg_match('/^\/project\/[a-z_-]+\/issues\/([0-9]+)$/', $url, $matches)) {
      return NULL;
    }

    return (int) $matches[1] ?? NULL;
  }

}
