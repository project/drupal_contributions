<?php

declare(strict_types = 1);

namespace Drupal\drupal_contributions\Contribution;

/**
 * Defines required methods for classes invoking the contribution client.
 */
interface ContributionClientInterface {

  /**
   * Get a list of contribution credit for issues.
   *
   * @param mixed $identifier
   *   A unique identifier to the resource where the contribution credit is
   *   being gathered from.
   *
   * @return array
   *   A list of projects and issues that were contributed to.
   */
  public function fetchProjects($identifier): array;

}
