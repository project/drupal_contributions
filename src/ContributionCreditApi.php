<?php

namespace Drupal\drupal_contributions;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\drupal_contributions\Contribution\ContributionClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an organization's Drupal contribution issue credits.
 */
class ContributionCreditApi {

  use StringTranslationTrait;

  /**
   * Cache system key for credits cache.
   */
  const CACHE_KEY_CREDITS = 'contribution_credits_cache';

  /**
   * The Contribution Client service.
   *
   * @var \Drupal\drupal_contributions\Contribution\ContributionClientInterface
   */
  protected $contributionClient;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Organization Group ID on Drupal.org.
   *
   * @var int
   */
  protected $organizationId;

  /**
   * Organization Group Title on Drupal.org.
   *
   * @var string
   */
  protected $organizationTitle;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Fetches and caches Drupal contribution data.
   *
   * @param \Drupal\drupal_contributions\Contribution\ContributionClientInterface $contribution_client
   *   The contribution client object to use.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state state.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(
    ContributionClientInterface $contribution_client,
    CacheBackendInterface $cache_backend,
    TranslationInterface $string_translation,
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    TimeInterface $time,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->contributionClient = $contribution_client;
    $this->cacheBackend = $cache_backend;
    $this->stringTranslation = $string_translation;
    $config = $config_factory->get('drupal_contributions.settings');
    $this->organizationId = $config->get('organization_id');
    $this->organizationTitle = $config->get('organization_title');
    $this->state = $state;
    $this->time = $time;
    $this->loggerFactory = $logger_factory->get('drupal_contributions');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_contributions.contribution_client'),
      $container->get('cache.contribution_cache'),
      $container->get('string_translation'),
      $container->get('state'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Fetch contribution credits via ContributionClient.
   *
   * @return array
   *   Return an array of issue credits for the contributing organization.
   */
  public function getCreditsFetch() {
    // Check if module is configured.
    if (!$this->configurationValid()) {
      // Log a warning.
      $this->loggerFactory->warning('Organization ID has not been configured.');
      return [];
    }
    // Restore projects from cache if available.
    if ($cached_data = $this->getCreditsCache()) {
      $projects = $cached_data['data'];
      $tags = $cached_data['tags'];
    }
    // Fetch all projects using the ContributionClient, then cache the data.
    else {
      $projects = $this->refreshIssueCreditData();
    }
    // Set the cache tags.
    $projects['#cache'] = [
      'tags' => $tags,
    ];
    return $projects;
  }

  /**
   * Fetch fresh contribution credits via ContributionClient.
   *
   * Refresh contribution credit data from Drupal.org and cache them locally.
   *
   * @return array
   *   Return an array of issue credits for the contributing organization.
   */
  public function refreshIssueCreditData() {
    $all_projects = $this->contributionClient->fetchProjects(
      $this->organizationId
    );
    // Set the cache tags.
    $tags = $all_projects['#cache']['tags'] ?? [];
    $tags = Cache::mergeTags($tags, [self::CACHE_KEY_CREDITS]);
    // Initialize arrays.
    $projects_core = [];
    $projects_contrib = [];
    // Loop through the fetched projects and create a list render array.
    foreach ($all_projects as $project_name => $values) {
      $issues = [];
      $project_link = Link::fromTextAndUrl($project_name, Url::fromUri($values['url']));
      // Create a list of issues per project.
      foreach ($values['issues'] as $issue) {
        $issue_link = Link::fromTextAndUrl($issue['title'], Url::fromUri($issue['url']));
        // Append the issue link with date to the issues array.
        $issues[] = [
          '#markup' => $this->t('%issue - @date', [
            '%issue' => $issue_link->toString(),
            '@date' => $this->formatIssueDate($issue['date']),
          ]),
        ];
      }
      // Save issue credits for Drupal core + contrib.
      if ($project_name == 'drupal') {
        $projects_core[] = [
          '#theme' => 'item_list',
          '#title' => $project_link,
          '#items' => $issues,
          '#variant' => 'drupal_contributions',
        ];
      }
      else {
        // Save issue credits for Drupal contributed modules.
        $projects_contrib[] = [
          '#theme' => 'item_list',
          '#title' => $project_link,
          '#items' => $issues,
          '#variant' => 'drupal_contributions',
        ];
      }
    }
    // Save core + contrib issues to the projects array.
    $projects['core'] = [
      '#theme' => 'item_list',
      '#title' => "$this->organizationTitle " . $this->t('Core Contributions'),
      '#items' => $projects_core,
      '#variant' => 'drupal_contributions',
    ];
    $projects['contrib'] = [
      '#theme' => 'item_list',
      '#title' => "$this->organizationTitle " . $this->t('Contributed Module Contributions'),
      '#items' => $projects_contrib,
      '#variant' => 'drupal_contributions',
    ];
    // Store the projects render array into the cache.
    $this->cacheBackend->set(self::CACHE_KEY_CREDITS, $projects, CacheBackendInterface::CACHE_PERMANENT, $tags);
    // Set the state of the last refresh.
    $this->state->set('drupal_contributions.last_refresh', $this->time->getCurrentTime());
    return $projects;
  }

  /**
   * Fetch contribution credits from cache.
   *
   * @return array|bool
   *   If the cached data exists, returns an array containing issue credits.
   *   Otherwise, returns false.
   */
  public function getCreditsCache() {
    if ($cached_data = $this->cacheBackend->get(self::CACHE_KEY_CREDITS)) {
      return [
        'data' => $cached_data->data,
        'tags' => $cached_data->tags,
      ];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Format date.
   *
   * @param string $date
   *   A date string.
   *
   * @return string
   *   A formatted date.
   */
  public function formatIssueDate(string $date) {
    $date_pieces = explode(' ', $date);
    return $date_pieces[1] . ' ' . $date_pieces[0] . ', ' . $date_pieces[2];
  }

  /**
   * Check if module is configured.
   *
   * @return bool
   *   True if organization id is set; false otherwise.
   */
  protected function configurationValid() {
    if (empty($this->organizationId)) {
      return FALSE;
    }
    return TRUE;
  }

}
