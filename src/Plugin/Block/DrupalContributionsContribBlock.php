<?php

namespace Drupal\drupal_contributions\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\drupal_contributions\ContributionCreditApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block for rendering Drupal contributed module issue credits.
 *
 * @Block(
 *   id = "drupal_contributions_contrib_block",
 *   admin_label = @Translation("Drupal Contributions - Contrib"),
 * )
 */
class DrupalContributionsContribBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The Contribution Credit API Service.
   *
   * @var \Drupal\drupal_contributions\ContributionCreditApi
   */
  protected $contributionCreditApi;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }

  /**
   * Initialize method.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition from discovery.
   * @param \Drupal\drupal_contributions\ContributionCreditApi $contribution_credit_api
   *   The contribution credit api service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ContributionCreditApi $contribution_credit_api
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->contributionCreditApi = $contribution_credit_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('drupal_contributions.contribution_credit_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Check for cached issue credits.
    if ($cached_data = $this->contributionCreditApi->getCreditsCache()) {
      // Get the Drupal contrib issue credits data.
      $projects = $cached_data['data'];
      return [
        '#theme' => 'drupal_contributions__contrib_block',
        '#issues' => [
          '#theme' => 'item_list',
          '#title' => $projects['contrib']['#title'],
          '#items' => $projects['contrib']['#items'],
          '#variant' => 'drupal_contributions',
        ],
      ];
    }
    else {
      return [
        '#theme' => 'drupal_contributions__loading_results_block',
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
