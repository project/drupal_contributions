<?php

declare(strict_types = 1);

namespace Drupal\drupal_contributions\Commands;

use Drupal\drupal_contributions\ContributionCreditApi;
use Drush\Commands\DrushCommands;

/**
 * Provides drush command for fetching projects from Drupal.org.
 */
class ContributionCommands extends DrushCommands {

  /**
   * The Contribution Credit API Service.
   *
   * @var \Drupal\drupal_contributions\ContributionCreditApi
   */
  protected $contributionCreditApi;

  /**
   * Constructs a new ContributionCommands object.
   *
   * @param \Drupal\drupal_contributions\ContributionCreditApi $contribution_credit_api
   *   The contribution credit api service.
   */
  public function __construct(
    ContributionCreditApi $contribution_credit_api
  ) {
    $this->contributionCreditApi = $contribution_credit_api;
  }

  /**
   * Fetches issue credits from Drupal.org.
   *
   * @usage drush contribution:fetch
   * @command contribution:fetch
   */
  public function fetch(): void {
    if ($this->contributionCreditApi->getCreditsFetch()) {
      $this->output()->writeln("<info>Fetched and cached issue credits from Drupal.org!</info>");
    }
    else {
      $this->output()->writeln("<error>Unable to fetch/cache issue credits from Drupal.org.</error>");
    }
  }

}
