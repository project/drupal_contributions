<?php

namespace Drupal\drupal_contributions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DrupalContributionsSettingsForm.
 *
 * This form manages configuration for pulling Drupal contributions.
 */
class DrupalContributionsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'drupal_contributions.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_contributions_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drupal_contributions.settings');
    $form['organization_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Organization ID'),
      '#default_value' => $config->get('organization_id'),
      '#required' => TRUE,
    ];
    $form['organization_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organization Title'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('organization_title'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('drupal_contributions.settings')
      ->set('organization_id', $form_state->getValue('organization_id'))
      ->set('organization_title', $form_state->getValue('organization_title'))
      ->save();
  }

}
