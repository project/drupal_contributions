Drupal Contributions
--------------------

### About this module

This module gets, caches, and renders an organization's core and contribution
issue credits from drupal.org.

After adding an organization's group id in the module's config settings, core
and contributed module issue credits can be fetched from drupal.org's issue
credit page for that organization via drush and cron.

Organizations on drupal.org have public facing issue credits pages using the
convention of https://www.drupal.org/node/[id]/issue-credits where [id]
represents the organization's group id. This module scrapes that page, saves the
data to cache, and provides rendering through the block plugin system.

### Installation

Install as you would normally install a contributed Drupal module.

### Configuration

1. Add the organization's Group ID to this module's configuration settings form.

2. You can now configure the module at `/admin/config/drupal_contributions/settings`.
