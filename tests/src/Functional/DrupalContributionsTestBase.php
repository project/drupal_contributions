<?php

namespace Drupal\Tests\drupal_contributions\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * drupal_contributions test setup base class.
 *
 * @group drupal_contributions
 */
abstract class DrupalContributionsTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['drupal_contributions'];

  /**
   * A user account with administration permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $permissions = [
      'administer drupal contributions',
      'access administration pages',
    ];
    $this->account = $this->createUser($permissions);
    $this->drupalLogin($this->account);
  }

}
