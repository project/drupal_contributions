<?php

namespace Drupal\Tests\drupal_contributions\Functional;

/**
 * Tests drupal_contributions links on modules listing page/form.
 *
 * @group drupal_contributions
 */
class DrupalContributionsModuleListingTest extends DrupalContributionsTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $permissions = [
      'administer drupal contributions',
      'administer modules',
      'access administration pages',
    ];
    $this->account = $this->createUser($permissions);
    $this->drupalLogin($this->account);
  }

  /**
   * Test for expected links on admin/modules listing page.
   */
  public function testModulesPageForm() {
    $this->drupalGet('/admin/modules');
    $this->assertResponse(200);

    $this->assertSession()->linkExists(t('Drupal Contributions'));
    $this->assertSession()->linkByHrefExists('admin/config/drupal_contributions/settings');
  }

}
