<?php

namespace Drupal\Tests\drupal_contributions\Functional;

/**
 * Tests drupal_contributions settings form.
 *
 * @group drupal_contributions
 */
class DrupalContributionsSettingsFormTest extends DrupalContributionsTestBase {

  /**
   * Test the settings page provided by drupal_contributions.
   */
  public function testSettingsFields() {
    $this->drupalGet('/admin/config/drupal_contributions/settings');
    $this->assertResponse(200);

    $this->assertSession()->pageContainsNoDuplicateId();

    $this->assertSession()->fieldExists(t('Organization ID'));
    $this->assertSession()->fieldExists('edit-organization-id');

    $this->assertSession()->fieldExists(t('Organization Title'));
    $this->assertSession()->fieldExists('edit-organization-title');
  }

  /**
   * Test settings form submission.
   */
  public function testSettingsFormSubmission() {
    $this->drupalGet('/admin/config/drupal_contributions/settings');
    $this->drupalPostForm(NULL, [
      'organization_id' => 1,
      'organization_title' => 'Company Name',
    ], t('Save configuration'));
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
  }

  /**
   * Test configuration page presence on admin/config listing page.
   */
  public function testSettingsFormListing() {
    $this->drupalGet('/admin/config');
    $this->assertResponse(200);

    $this->assertSession()->linkExists(t('Drupal Contributions'));
    $this->assertSession()->linkByHrefExists('admin/config/drupal_contributions/settings');
    $this->assertSession()->pageTextContains('Configure tracking of Drupal.org contributions.');
  }

}
